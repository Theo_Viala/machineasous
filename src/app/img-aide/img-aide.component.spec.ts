import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImgAideComponent } from './img-aide.component';

describe('ImgAideComponent', () => {
  let component: ImgAideComponent;
  let fixture: ComponentFixture<ImgAideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImgAideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImgAideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
