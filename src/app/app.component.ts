import { Component, OnInit } from '@angular/core';
import { TirageService } from './tirage.service'
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private TirageService: TirageService) { }
  title = 'MachineASous';
  ticket = 0;
  flex = "inline-flex";
  mise = 1;

  resultPos1: string = "";
  resultPos2: string = "";
  resultPos3: string = "";

  ngOnInit() {
    this.resultPos1 = this.TirageService.TirageRandom();
    this.resultPos2 = this.TirageService.TirageRandom();
    this.resultPos3 = this.TirageService.TirageRandom();
    this.ticket = 10;

  }


  LancerTirage() {
    let resultTirage = this.TirageService.TirageForwinLose();
    if (resultTirage == 10) {
      this.resultPos1 = this.TirageService.TirageRandom();
      this.resultPos2 = this.TirageService.TirageRandom();
      this.resultPos3 = this.TirageService.TirageRandom();
    } else {
      this.ticket += this.TirageService.ticketWin;
      this.resultPos1 = resultTirage
      this.resultPos2 = resultTirage
      this.resultPos3 = resultTirage

    }
  }




  AnimationTirage() {
    if (this.ticket > 0 && this.mise <= this.ticket && this.mise !=0 && Number.isInteger(+this.mise)) {
      for (let i = 80; i > 0; i--) {

        this.TirageService.delay(this.TirageService.randomDelay()).then(any => {
          this.resultPos1 = this.TirageService.TirageRandom();

        });
        this.TirageService.delay(this.TirageService.randomDelay()).then(any => {
          this.resultPos2 = this.TirageService.TirageRandom();

        });
        this.TirageService.delay(this.TirageService.randomDelay()).then(any => {
          this.resultPos3 = this.TirageService.TirageRandom();

        });

      }


      this.ticket = this.ticket - this.mise;
      this.TirageService.delay(1100).then(any => {
        this.LancerTirage();

      });


    }
  }


  SetMise(input) {
    if (this.mise <= this.ticket && this.mise !=0 && Number.isInteger(+this.mise)) {
    this.mise = input
    this.TirageService.mise = this.mise
  }
}
}
