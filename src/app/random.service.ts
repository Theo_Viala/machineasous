import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RandomService {

  constructor() { }

   getRandomInt(min,max) {
    return Math.floor(Math.random() * (+max - +min)) + +min;
  }
}

