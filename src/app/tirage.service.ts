import { Injectable } from '@angular/core';
import { RandomService } from './random.service'
@Injectable({
  providedIn: 'root'
})
export class TirageService {

  constructor(private random: RandomService) { }

  randomInt = 0;
  ticketWin = 0
  mise = 1;
  TirageRandom() {
    this.randomInt = this.random.getRandomInt(0, 8);
    let ElementChoisi = "../assets/Icones/" + this.randomInt + ".jpg"
    return ElementChoisi;
  }


  TirageForwinLose() {
    let randomproba = this.random.getRandomInt(0, 1000)
    console.log(randomproba)
    let Element
    // victoire 777 proba 1/500
    if (randomproba < 2) {
      Element = "../assets/Icones/" + 1 + ".jpg"
      this.ticketWin = 50 * this.mise
      return Element;
    }
    // victoire Diamant proba 1/333
    if (randomproba < 5 && randomproba >= 2) {
      Element = "../assets/Icones/" + 5 + ".jpg"
      this.ticketWin = 25 * this.mise
      return Element;
    }
    // victoire etoile proba 1/200
    if (randomproba < 10 && randomproba >= 5) {
      Element = "../assets/Icones/" + 6 + ".jpg"
      this.ticketWin = 15 * this.mise
      return Element;
    }

    // victoire Cloche proba 1/125
    if (randomproba < 18 && randomproba >= 10) {
      Element = "../assets/Icones/" + 4 + ".jpg"
      this.ticketWin = 10 * this.mise
      return Element;
    }
    // victoire Cerise proba 1/83
    if (randomproba < 30 && randomproba >= 18) {
      Element = "../assets/Icones/" + 3 + ".jpg"
      this.ticketWin = 9 * this.mise
      return Element;
    }
    // victoire Raisin proba 1/50
    if (randomproba < 50 && randomproba >= 30) {
      Element = "../assets/Icones/" + 0 + ".jpg"
      this.ticketWin = 8 * this.mise
      return Element;
    }
    // victoire Orange proba 1/33
    if (randomproba < 80 && randomproba >= 50) {
      Element = "../assets/Icones/" + 7 + ".jpg"
      this.ticketWin = 6 * this.mise
      return Element;
    }
    // victoire Pasteque  proba 1/25
    if (randomproba < 120 && randomproba >= 80) {
      Element = "../assets/Icones/" + 8 + ".jpg"
      this.ticketWin = 5 * this.mise
      return Element;
    }
    // victoire Bar proba 1/12.5
    if (randomproba < 200 && randomproba >= 120) {
      Element = "../assets/Icones/" + 2 + ".jpg"
      this.ticketWin = 2 * this.mise

      return Element;
    }

    //Echec
    if (randomproba >= 200) {
      return 10
    }

  }
  getTicketWin() {
    return this.ticketWin;
  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }


  randomDelay() {
    let randomDelayInt = this.random.getRandomInt(100, 1000);
    return randomDelayInt;

  }
}
